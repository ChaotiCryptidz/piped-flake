{
  lib,
  rustPlatform,
  fetchFromGitHub,
  nasm,
  openssl,
  pkg-config,
  withOpenSSL ? false,
  withMimalloc ? true,
  withWebP ? true,
  withAVIF ? false,
}: let
  meta = builtins.fromJSON (builtins.readFile ../../meta.json);
  rev = meta.proxy.rev;
  inherit (lib.lists) optional;
in
  rustPlatform.buildRustPackage rec {
    pname = "piped-proxy";
    version = "latest-${rev}";
    src = fetchFromGitHub {
      owner = "TeamPiped";
      repo = "piped-proxy";
      inherit rev;
      sha256 = "${meta.proxy.sha256}";
    };

    buildNoDefaultFeatures = true;
    buildFeatures =
      (
        if withOpenSSL
        then ["reqwest-native-tls"]
        else ["reqwest-rustls"]
      )
      ++ (optional withMimalloc "mimalloc")
      ++ (optional withAVIF "avif")
      ++ (optional withWebP "webp");

    buildInputs = optional withOpenSSL openssl;
    nativeBuildInputs = [] ++ (optional withAVIF nasm) ++ (optional withOpenSSL pkg-config);

    cargoLock = {lockFile = "${src}/Cargo.lock";};
    doCheck = false;
  }
