{
  lib,
  stdenv,
  fetchFromGitHub,
  fetchurl,
  gradle,
  jdk,
  perl,
}: let
  meta = builtins.fromJSON (builtins.readFile ../../meta.json);

  extraDeps = [
    {
      filename = "okio-3.2.0.jar";
      path = "com/squareup/okio/okio/3.2.0";
      url = "https://repo1.maven.org/maven2/com/squareup/okio/okio/3.2.0/okio-3.2.0.jar";
      sha256 = "sha256-3KkyyyAptsniZ3D4fbCLFNSB/+gTGlnzaaI4XBG+Ti0=";
    }
  ];
in
  stdenv.mkDerivation {
    pname = "piped-backend-deps";
    version = "latest-${meta.backend.rev}";

    src = fetchFromGitHub {
      owner = "TeamPiped";
      repo = "Piped-Backend";
      rev = "${meta.backend.rev}";
      sha256 = "${meta.backend.sha256}";
    };

    nativeBuildInputs = [gradle jdk perl];

    buildPhase = ''
      export JAVA_HOME=${jdk}
      export GRADLE_USER_HOME=$(mktemp -d);
      gradle -P org.gradle.java.installations.fromEnv=JAVA_HOME --no-daemon assemble shadowJar
    '';

    # perl code mavenizes paths (com.squareup.okio/okio/1.13.0/a9283170b7305c8d92d25aff02a6ab7e45d06cbe/okio-1.13.0.jar -> com/squareup/okio/okio/1.13.0/okio-1.13.0.jar)
    installPhase =
      ''
        find $GRADLE_USER_HOME/caches/modules-2 -type f -regex '.*\.\(jar\|pom\)' \
          | perl -pe 's#(.*/([^/]+)/([^/]+)/([^/]+)/[0-9a-f]{30,40}/([^/\s]+))$# ($x = $2) =~ tr|\.|/|; "install -Dm444 $1 \$out/$x/$3/$4/$5" #e' \
          | sh
      ''
      + lib.concatStringsSep "\n" (lib.forEach extraDeps (dep: ''
        mkdir -p $out/${dep.path}
        cp ${fetchurl {
          url = dep.url;
          sha256 = dep.sha256;
        }} $out/${dep.path}/${dep.filename}
      ''));

    dontStrip = true;

    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = "${meta.backend.deps-sha256}";
  }
