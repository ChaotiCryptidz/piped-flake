{
  stdenv,
  runtimeShell,
  fetchFromGitHub,
  jdk,
  jre,
  gradle,
  perl,
  writeText,
  callPackage,
  piped-backend-deps,
}: let
  meta = builtins.fromJSON (builtins.readFile ../../meta.json);

  deps = piped-backend-deps;

  gradleInit = writeText "init.gradle" ''
    logger.lifecycle 'Replacing Maven repositories with ${deps}...'
    gradle.projectsLoaded {
      rootProject.allprojects {
        buildscript {
          repositories {
            clear()
            maven { url '${deps}' }
          }
        }
        repositories {
          clear()
          maven { url '${deps}' }
        }
      }
    }
    settingsEvaluated { settings ->
      settings.pluginManagement {
        repositories {
          maven { url '${deps}' }
        }
      }
    }
  '';
in
  stdenv.mkDerivation rec {
    pname = "piped-backend";
    version = "latest-${meta.backend.rev}";

    src = fetchFromGitHub {
      owner = "TeamPiped";
      repo = "Piped-Backend";
      rev = "${meta.backend.rev}";
      sha256 = "${meta.backend.sha256}";
    };

    nativeBuildInputs = [gradle jdk];

    buildPhase = ''
      runHook preBuild

      export JAVA_HOME=${jdk}
      export GRADLE_USER_HOME=$(mktemp -d)

      gradle -P org.gradle.java.installations.fromEnv=JAVA_HOME --offline --init-script ${gradleInit} shadowJar

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      ls -R build

      mkdir -p "$out/share/piped-backend"
      cp build/libs/piped-1.0-all.jar "$out/share/piped-backend"

      mkdir -p "$out/bin"
      cat  <<EOF >$out/bin/piped-backend
      #!${runtimeShell}
      export JAVA_HOME=${jre}
      exec ${jre}/bin/java -jar "$out/share/piped-backend/piped-1.0-all.jar" "\$@"
      EOF
      chmod a+x "$out/bin/piped-backend"

      runHook postInstall
    '';
  }
