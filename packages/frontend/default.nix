{
  mkYarnPackage,
  rsync,
  fetchFromGitHub,
  # Backend domain override, if unset then use project default
  backendDomain ? null,
}: let
  meta = builtins.fromJSON (builtins.readFile ../../meta.json);
  rev = meta.frontend.rev;
in
  mkYarnPackage rec {
    pname = "piped-frontend";
    version = "latest-${rev}";
    src = fetchFromGitHub {
      owner = "TeamPiped";
      repo = "Piped";
      inherit rev;
      sha256 = "${meta.frontend.sha256}";
    };

    packageJSON = "${src}/package.json";
    yarnLock = ./yarn.lock;
    yarnNix = ./yarn.nix;

    patchPhase = ''
      ${
        if backendDomain != null
        then ''
          sed -i "s#pipedapi.kavin.rocks#${backendDomain}#g" src/main.js
          sed -i "s#pipedapi.kavin.rocks#${backendDomain}#g" src/components/PreferencesPage.vue
        ''
        else ""
      }
    '';

    buildPhase = ''
      runHook preBuild
      cp ${./yarn.lock} yarn.lock
      yarn --offline build
      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      mkdir -p "$out/share/piped-frontend"
      ${rsync}/bin/rsync --recursive deps/piped/dist/ "$out/share/piped-frontend"

      runHook postInstall
    '';

    doDist = false;
  }
