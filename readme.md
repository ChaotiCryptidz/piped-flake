# Piped-Flake
This is a flake which allows you to run Piped on NixOS.

This should allow for more advanced and declarative configuration than the upstream docker containers. It also includes more documentation on config files.

## How to run

This should provide a working piped instance.

You can look at the options in module/default.nix for more information.

```nix
nixpkgs.overlays = [ inputs.piped-flake.overlays.default ];
imports = [ inputs.piped-flake.nixosModules.default ];

services.piped = let 
  baseDomain = "example.org";
in {
  enable = true;

  frontend = {
    domain = "${baseDomain}";
  };

  backend = {
    domain = "backend.${baseDomain}";    
  };

  proxy = {
    domain = "proxy.${baseDomain}";
  };
}
```

## Supported Systems

`x86_64-linux` and `aarch64-linux` are both supported

However if you are deploying to `aarch64-linux` from a non-arm64 host via `qemu-user`` or using aarch64 builders without `--max-jobs=0` then the build of piped-backend will fail.

https://github.com/NixOS/nixpkgs/issues/255780

This appears to be a problem with upstream gradle.

For now you will have to build on a `aarch64-linux` host or use one as builder with `--max-jobs=0` to not use local host to build.

You can use the below on a `aarch64-linux` host to build and copy the built backend to your computer.

```sh
nix build .#piped-backend --system aarch64-linux
nix-copy-closure --to root@host-ip --use-substitutes $(readlink result)
```