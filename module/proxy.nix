{
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkIf mkDefault;

  cfg = config.services.piped;

  proxyConfig = cfg.proxy;
  nginxConfig = proxyConfig.nginx;

  defaultNginxExtraConfig = ''
    proxy_buffering on;
    proxy_buffers 1024 16k;
    proxy_set_header X-Forwarded-For "";
    proxy_set_header CF-Connecting-IP "";
    proxy_hide_header "alt-svc";
    sendfile on;
    sendfile_max_chunk 512k;
    tcp_nopush on;
    aio threads=default;
    aio_write on;
    directio 16m;
    proxy_hide_header Cache-Control;
    proxy_hide_header etag;
    proxy_http_version 1.1;
    proxy_set_header Connection keep-alive;
    proxy_max_temp_file_size 32m;
    access_log off;
  '';
in {
  config = mkIf (proxyConfig.enable) {
    systemd.services.piped-proxy = {
      wantedBy = ["multi-user.target"];
      environment.BIND = "0.0.0.0:${toString proxyConfig.internalPort}";
      environment.IPV4_ONLY = mkIf proxyConfig.proxyIPv4Only "1";
      environment.DISALLOW_IMAGE_TRANSCODING = mkIf proxyConfig.disallowImageTranscoding "1";
      serviceConfig = {
        ExecStart = "${proxyConfig.package}/bin/piped-proxy";

        RestartSec = "5s";
        User = "piped";

        CapabilityBoundingSet = "";
        PrivateDevices = true;
        PrivateUsers = true;
        ProtectHome = true;
        ProtectKernelLogs = true;
        ProtectProc = "invisible";
        RestrictAddressFamilies = ["AF_UNIX" "AF_INET" "AF_INET6"];
        RestrictNamespaces = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = ["@system-service" "~@privileged" "~@resources"];
      };
    };

    services.nginx = mkIf (!nginxConfig.disableNginx) {
      enable = true;

      virtualHosts."${proxyConfig.domain}" = {
        forceSSL = mkDefault nginxConfig.forceSSL;
        enableACME = mkDefault nginxConfig.enableACME;

        locations."/" = {
          proxyPass = "http://localhost:${toString proxyConfig.internalPort}";
          extraConfig = ''
            ${defaultNginxExtraConfig}
            add_header Cache-Control "public, max-age=604800";
          '';
        };

        locations."~ (/videoplayback|/api/v4/|/api/manifest/)" = {
          proxyPass = "http://localhost:${toString proxyConfig.internalPort}";
          extraConfig = ''
            ${defaultNginxExtraConfig}
            add_header Cache-Control private always;
          '';
        };
      };
    };
  };
}
