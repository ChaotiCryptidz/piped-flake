{
  config,
  lib,
  ...
}: let
  inherit (lib.modules) mkIf mkDefault;

  cfg = config.services.piped;

  frontendConfig = cfg.frontend;
  nginxConfig = frontendConfig.nginx;

  frontendPackage = frontendConfig.package.override {
    backendDomain = cfg.backend.domain;
  };
in {
  config = mkIf (frontendConfig.enable) {
    # https://github.com/TeamPiped/Piped/blob/master/docker/nginx.conf
    services.nginx = {
      enable = true;

      virtualHosts."${frontendConfig.domain}" = {
        forceSSL = mkDefault nginxConfig.forceSSL;
        enableACME = mkDefault nginxConfig.enableACME;

        locations."/" = {
          root = "${frontendPackage}/share/piped-frontend";
          index = "index.html index.htm";
        };
        # I have no idea why try_files for Single Page Apps doesn't work here
        extraConfig = ''
          error_page 404 =200 /index.html;
        '';
      };
    };
  };
}
