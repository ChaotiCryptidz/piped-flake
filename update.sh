#!/usr/bin/env nix-shell
#!nix-shell -i bash -p curl jq git moreutils nodejs_20 yarn2nix yarn nix
set -euo pipefail

BASE_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "${BASE_DIR}"

json_get() {
    jq -r "$1" < 'meta.json'
}

json_set() {
    jq --arg x "$2" "$1 = \$x" < 'meta.json' | sponge 'meta.json'
}

# Frontend
old_frontend_rev=$(json_get '.frontend.rev')
new_frontend_rev=$(curl -L "https://api.github.com/repos/TeamPiped/Piped/commits" 2>/dev/null | jq ".[0].sha" -r)
if [ "$new_frontend_rev" != "$old_frontend_rev" ] || [ "${UPDATE-}" == "frontend" ]; then
    echo "Frontend is out of date. Updating..."
    json_set '.frontend.rev' "$new_frontend_rev"
    json_set '.frontend.sha256' ""

    TMP=$(mktemp -d)
    pushd "$TMP"
        git clone https://github.com/TeamPiped/Piped
        pushd Piped
            git reset --hard "$new_frontend_rev"
            #yarn install --no-lockfile
            # yarn add unocss@0.56.4
            yarn install --mode update-lockfile
            nix run "github:NixOS/nixpkgs/nixos-unstable#yarn2nix" > "${BASE_DIR}/packages/frontend/yarn.nix"
            cp yarn.lock "${BASE_DIR}/packages/frontend/yarn.lock"
        popd
    popd
    rm -rf "$TMP"
fi

# Backend
old_backend_rev=$(json_get '.backend.rev')
new_backend_rev=$(curl -L "https://api.github.com/repos/TeamPiped/Piped-Backend/commits" 2>/dev/null | jq ".[0].sha" -r)
if [ "$new_backend_rev" != "$old_backend_rev" ] || [ "${FORCE_UPDATE-}" == "backend" ]; then
    echo "Backend is out of date. Updating..."
    json_set '.backend.rev' "$new_backend_rev"
    json_set '.backend.sha256' ""
    json_set '.backend."deps-sha256"' ""
fi

# Proxy
old_proxy_rev=$(json_get '.proxy.rev')
new_proxy_rev=$(curl -L "https://api.github.com/repos/TeamPiped/piped-proxy/commits" 2>/dev/null | jq ".[0].sha" -r)
if [ "$new_proxy_rev" != "$old_proxy_rev" ] || [ "${FORCE_UPDATE-}" == "proxy" ]; then
    echo "Proxy is out of date. Updating..."
    json_set '.proxy.rev' "$new_proxy_rev"
    json_set '.proxy.sha256' ""
fi

# gotta manually update shasums using output from these 
echo "building frontend"
nix build .#piped-frontend || true

echo "building backend"
nix build .#piped-backend || true

