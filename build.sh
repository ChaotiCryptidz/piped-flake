#!/usr/bin/env bash
set -euo pipefail

BASE_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "${BASE_DIR}"

rm -rf result
mkdir result

echo "Building Frontend"
nix build -v .#piped-frontend -o result/piped-frontend "$@"

echo "Building Backend"
nix build -v .#piped-backend -o result/piped-backend "$@"

echo "Building Proxy"
nix build -v .#piped-proxy -o result/piped-proxy "$@"
