{
  description = "Piped - A alternate frontend for YouTube";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    ...
  }:
    {
      nixosModules.piped = import ./module/default.nix;
      nixosModules.default = self.nixosModules.piped;

      overlays.piped = final: _prev: rec {
        piped-frontend = final.callPackage ./packages/frontend {};
        piped-backend = final.callPackage ./packages/backend {
          jre = final.openjdk19_headless;
          jdk = final.openjdk19;
        };
        piped-proxy = final.callPackage ./packages/proxy {};
	piped-proxy-full =  piped-proxy.override {withAVIF = true;};
	piped-proxy-full-openssl =  piped-proxy-full.override {withOpenSSL = true;};
        piped-proxy-openssl = piped-proxy.override {withOpenSSL = true;};
        piped-proxy-minimal = piped-proxy.override {
          withAVIF = false;
          withWebP = false;
        };
        piped-proxy-minimal-openssl = piped-proxy-minimal.override {withOpenSSL = true;};
        piped-backend-deps = final.callPackage ./packages/backend/deps.nix {
          jdk = final.openjdk19;
        };
      };
      overlays.default = self.overlays.piped;
    }
    // utils.lib.eachSystem (utils.lib.defaultSystems) (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [self.overlays.default];
      };
    in {
      formatter = pkgs.alejandra;

      packages = {
        inherit (pkgs) piped-frontend;
        inherit (pkgs) piped-backend;
        inherit (pkgs) piped-proxy piped-proxy-openssl;
        inherit (pkgs) piped-proxy-full piped-proxy-full-openssl;
        inherit (pkgs) piped-proxy-minimal piped-proxy-minimal-openssl;
      };
    });
}
